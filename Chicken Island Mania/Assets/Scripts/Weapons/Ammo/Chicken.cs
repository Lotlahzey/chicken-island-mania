﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chicken : MonoBehaviour 
{
	[Header("Stats")]
	[Space(2)]
	//Projectile stats
	public float damage = 1;
	public float lifespan = 5f;

	[Header("Explosive Options")]
	[Space(2)]
	//Stats for explosive projectiles
	public bool explosive = false;
	public GameObject shrapnel;
	public int fragments = 3;
	public int explosiveForce = 250;

	private float time;

	void Start () 
	{
		time = 0;
	}
	
	void Update () 
	{
		//Destroys projectile based on lifespan
		time += Time.deltaTime;
		if (time > lifespan) 
		{
			Destroy (this.gameObject);
		}
	}

	void OnCollisionEnter (Collision other) 
	{
		//Defines explosion for explosive projectiles
		if (explosive) 
		{
			for (int i = 0; i < fragments; i++) 
			{
				Destroy (gameObject);
				GameObject clone = Instantiate (shrapnel, gameObject.transform.position, gameObject.transform.rotation);
				Rigidbody rb = clone.GetComponent<Rigidbody> ();
				rb.AddForce (gameObject.transform.up * explosiveForce);
			}
		} 
	}
}
