﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sniper : MonoBehaviour 
{
	private bool zoom = false;

	public GameObject projectile;

	[Space(5)]
	public Transform firingPoint;

	[Header("Stats")]
	[Space(2)]
	//Weapon stats
	public float force = 1800;
	public int ammo;
	public int maxAmmo = 3;

	void Start () 
	{
		//Sets variable weapon stats
		ammo = maxAmmo;
	}
		
	void Update () 
	{
		//Shooting
		if (ammo > 0) 
		{
			if (Input.GetMouseButtonDown (0)) 
			{
				Fire ();
			}
		}

		//Zoom
		if (Input.GetMouseButtonDown (1)) 
		{
			zoom = !zoom;
		}
		if (zoom) 
		{
			GameManager.gm.player.playerCamera.fieldOfView = Mathf.Lerp (GameManager.gm.player.playerCamera.fieldOfView, 20, Time.deltaTime * 10);

		} 
		else if (!zoom) 
		{
			GameManager.gm.player.playerCamera.fieldOfView = Mathf.Lerp (GameManager.gm.player.playerCamera.fieldOfView, 60, Time.deltaTime * 10);
		}

		//Reloading
		if (Input.GetKeyDown (KeyCode.R)) 
		{
			Reload ();
		}
	}

	//Shoots projectile
	void Fire () 
	{
		AudioSource.PlayClipAtPoint (GameManager.gm.player.gunSound, firingPoint.position, GameManager.gm.audioManager.soundEffectsVolume);
		GameObject clone = Instantiate (projectile, firingPoint.position, firingPoint.rotation);
		Rigidbody rb = clone.GetComponent<Rigidbody> ();
		rb.AddForce (firingPoint.forward * force);
		ammo--;
	}

	//Resets ammo
	void Reload () 
	{
		ammo = maxAmmo;
	}
}
