﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour 
{
	//Holds projectile prefab
	public GameObject projectile;

	[Space(5)]
	//Holds location where projectile is fired from the gun
	public Transform firingPoint;

	[Header("Stats")]
	[Space(2)]
	//Weapon stats
	public float force;
	public int ammo;
	public int maxAmmo = 2;
	public float minCharge = 500;
	public float maxCharge = 1500;
	public float rateOfCharge = 10;

	void Start () 
	{
		//Sets variable weapon stats
		force = minCharge;
		ammo = maxAmmo;
	}

	void Update () 
	{
		//Charging and shooting
		if (ammo > 0) 
		{
			if (Input.GetMouseButton (0)) 
			{
				Charge ();
			} 
			else if (Input.GetMouseButtonUp (0)) 
			{
				Fire ();
			}
		}

		//Reloading
		if (Input.GetKeyDown (KeyCode.R) && !Input.GetMouseButton (0)) 
		{
			Reload ();
		}
	}
		
	//Increases weapon charge
	void Charge () 
	{
		if (force < maxCharge) 
		{
			force += 1 * rateOfCharge;
		}
	}
		
	//Shoots projectile
	void Fire () 
	{
		AudioSource.PlayClipAtPoint (GameManager.gm.player.gunSound, firingPoint.position, GameManager.gm.audioManager.soundEffectsVolume);
		GameObject clone = Instantiate (projectile, firingPoint.position, firingPoint.rotation);
		Rigidbody rb = clone.GetComponent<Rigidbody> (); 
		rb.AddForce (firingPoint.forward * force);
		force = minCharge;
		ammo--;
	}

	//Resets weapon ammo
	void Reload () 
	{
		ammo = maxAmmo;
	}
}
