﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : MonoBehaviour 
{
	public GameObject projectile;

	[Space(5)]
	public Transform firingPoint;

	[Header("Stats")]
	[Space(2)]
	//Weapon stats
	public float force = 600;
	public int ammo;
	public int maxAmmo = 6;

	void Start () 
	{
		//Sets variable weapon stats
		ammo = maxAmmo;
	}
	
	void Update () 
	{
		//Shooting
		if (ammo > 0) 
		{
			if (Input.GetMouseButtonDown (0)) 
			{
				Fire ();
			}
		}

		//Reloading
		if (Input.GetKeyDown (KeyCode.R)) 
		{
			Reload ();
		}
	}

	//Shoots projectile
	void Fire () 
	{
		AudioSource.PlayClipAtPoint (GameManager.gm.player.gunSound, firingPoint.position, GameManager.gm.audioManager.soundEffectsVolume);
		GameObject clone = Instantiate (projectile, firingPoint.position, firingPoint.rotation);
		Rigidbody rb = clone.GetComponent<Rigidbody> ();
		rb.AddForce (firingPoint.forward * force);
		ammo--;
	}

	//Resets ammo
	void Reload () 
	{
		ammo = maxAmmo;
	}
}
