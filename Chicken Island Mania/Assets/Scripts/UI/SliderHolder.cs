﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderHolder : MonoBehaviour 
{
	//Paths to access slider UI elements
	public Slider musicVolume;
	public Slider soundVolume;
}
