﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserInterfaceManager : MonoBehaviour 
{
	[Header("Game Screens")]
	[Space(2)]
	//Paths to access the different UI menus
	public UIScreen startMenu;
	public UIScreen backgroundUI;
	public UIScreen inventoryScreen;
	public UIScreen pauseScreen;
	public UIScreen optionsScreen;
	public UIScreen gameOverScreen;

	[Space(5)]
	//The camera used when viewing a menu
	public Camera menuCamera;
	
	void Update () 
	{
		//Updates score counter and life counter in real-time
		backgroundUI.canvas.gameObject.GetComponent<StatsTextHolder> ().scoreCounter.text = GameManager.gm.player.score.ToString ();
		backgroundUI.canvas.gameObject.GetComponent<StatsTextHolder> ().lifeCounter.text = GameManager.gm.player.lives.ToString ();

		//Shows the stats of the weapon the player has equipped
		ShowWeaponStats ();
	}

	//Sets the game's background UI
	public void SetGameplayScreen () 
	{
		menuCamera.gameObject.SetActive (false);
		Cursor.visible = false;	
		startMenu.gameObject.SetActive (false);
		inventoryScreen.gameObject.SetActive (false);
		pauseScreen.gameObject.SetActive (false);
		optionsScreen.gameObject.SetActive (false);
		backgroundUI.gameObject.SetActive (true);
	}

	//Sets the inventory screen
	public void SetInventoryScreen ()
	{
		menuCamera.gameObject.SetActive (true);
		Cursor.visible = true;
		backgroundUI.gameObject.SetActive (false);
		inventoryScreen.gameObject.SetActive (true);
	}

	//Sets the pause screen
	public void SetPauseScreen ()
	{
		menuCamera.gameObject.SetActive (true);
		Cursor.visible = true;
		backgroundUI.gameObject.SetActive (false);
		inventoryScreen.gameObject.SetActive (false);
		optionsScreen.gameObject.SetActive (false);
		pauseScreen.gameObject.SetActive (true);
	}

	//Sets the options screen
	public void SetOptionsScreen ()
	{
		pauseScreen.gameObject.SetActive (false);
		optionsScreen.gameObject.SetActive (true);
	}

	//Sets the game over screen 
	public void SetGameOverScreen () 
	{
		menuCamera.gameObject.SetActive (true);
		Cursor.visible = true;
		backgroundUI.gameObject.SetActive (false);
		gameOverScreen.gameObject.SetActive (true);
	}

	//Sets the buttons for the player's weapons in inventory
	public void SetWeaponsInInvetory () 
	{
		if (GameManager.gm.player.hasPistol) 
		{
			inventoryScreen.canvas.GetComponent<ButtonHolder> ().pistol.interactable = true;
		}
		if (GameManager.gm.player.hasFlamethrower) 
		{
			inventoryScreen.canvas.GetComponent<ButtonHolder> ().flamethrower.interactable = true;
		}
		if (GameManager.gm.player.hasSniper) 
		{
			inventoryScreen.canvas.GetComponent<ButtonHolder> ().sniper.interactable = true;
		}
		if (GameManager.gm.player.hasShotgun) 
		{
			inventoryScreen.canvas.GetComponent<ButtonHolder> ().shotgun.interactable = true;
		}
		if (GameManager.gm.player.hasCannon) 
		{
			inventoryScreen.canvas.GetComponent<ButtonHolder> ().cannon.interactable = true;
		}
	}

	//Displays weapon stats on UI for the weapon equipped
	public void ShowWeaponStats () 
	{
		if (GameManager.gm.player.chickenPistol.gameObject.activeInHierarchy) 
		{
			backgroundUI.canvas.GetComponent<StatsTextHolder> ().ammoCounter.text = GameManager.gm.player.chickenPistol.ammo.ToString ();
			backgroundUI.canvas.GetComponent<StatsTextHolder> ().powerCounter.text = GameManager.gm.player.chickenPistol.force.ToString ();
		}
		if (GameManager.gm.player.chickenFlinger.gameObject.activeInHierarchy) 
		{
			backgroundUI.canvas.GetComponent<StatsTextHolder> ().ammoCounter.text = GameManager.gm.player.chickenFlinger.ammo.ToString ();
			backgroundUI.canvas.GetComponent<StatsTextHolder> ().powerCounter.text = GameManager.gm.player.chickenFlinger.force.ToString ();
		}
		if (GameManager.gm.player.chickenSniper.gameObject.activeInHierarchy) 
		{
			backgroundUI.canvas.GetComponent<StatsTextHolder> ().ammoCounter.text = GameManager.gm.player.chickenSniper.ammo.ToString ();
			backgroundUI.canvas.GetComponent<StatsTextHolder> ().powerCounter.text = GameManager.gm.player.chickenSniper.force.ToString ();
		}
		if (GameManager.gm.player.chickenBlaster.gameObject.activeInHierarchy) 
		{
			backgroundUI.canvas.GetComponent<StatsTextHolder> ().ammoCounter.text = GameManager.gm.player.chickenBlaster.ammo.ToString ();
			backgroundUI.canvas.GetComponent<StatsTextHolder> ().powerCounter.text = GameManager.gm.player.chickenBlaster.force.ToString ();

		}
		if (GameManager.gm.player.chickenBomber.gameObject.activeInHierarchy) 
		{
			backgroundUI.canvas.GetComponent<StatsTextHolder> ().ammoCounter.text = GameManager.gm.player.chickenBomber.ammo.ToString ();
			backgroundUI.canvas.GetComponent<StatsTextHolder> ().powerCounter.text = GameManager.gm.player.chickenBomber.force.ToString ();
		}
	}
}
