﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsTextHolder : MonoBehaviour 
{
	//Paths to access text UI elements
	public Text scoreCounter;
	public Text lifeCounter;
	public Text ammoCounter;
	public Text powerCounter;
}
