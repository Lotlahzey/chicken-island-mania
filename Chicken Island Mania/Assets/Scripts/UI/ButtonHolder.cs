﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonHolder : MonoBehaviour 
{
	//Paths to acess UI button elements
	public Button pistol;
	public Button flamethrower;
	public Button sniper;
	public Button shotgun;
	public Button cannon;
}
