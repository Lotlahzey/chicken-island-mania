﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour 
{
	[HideInInspector]
	public float soundEffectsVolume;

	void Update () 
	{
		//Sets volume in game in accordance to slider position in options
		GameManager.gm.player.GetComponent<AudioSource> ().volume = GameManager.gm.userInterfaceManager.optionsScreen.canvas.GetComponent<SliderHolder> ().soundVolume.value;
		soundEffectsVolume = GameManager.gm.userInterfaceManager.optionsScreen.canvas.GetComponent<SliderHolder> ().soundVolume.value;
		GameManager.gm.audioManager.GetComponent<AudioSource>().volume = GameManager.gm.userInterfaceManager.optionsScreen.canvas.GetComponent<SliderHolder> ().musicVolume.value;
	}
}
