﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour 
{
	public GameObject enemy;
	public float spawnInterval = 5f;

	private float time;
	private bool spawnCooldown;

	void Start () 
	{
		spawnCooldown = true;
	}
	
	void Update () 
	{
		//Enemy spawning
		time += Time.deltaTime;
		if (time > spawnInterval) 
		{
			
			Instantiate (enemy, transform.position, transform.rotation);
			time = 0;
		}

		//Increasing spawn rate as score increases
		if (GameManager.gm.player.score % 10 == 0 && GameManager.gm.player.score != 0 && spawnCooldown == true) 
		{
			if (spawnInterval > 1) 
			{
		        spawnInterval--;
				spawnCooldown = false;
		    }
		}
		if (GameManager.gm.player.score % 10 == 1) 
		{
			spawnCooldown = true;
		}
	}
}
