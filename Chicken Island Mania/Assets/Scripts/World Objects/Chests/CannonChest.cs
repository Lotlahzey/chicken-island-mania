﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonChest : MonoBehaviour 
{
	void OnTriggerEnter (Collider other) 
	{
		if (other.gameObject.CompareTag ("Player"))
		{
			//Gives player the cannon weaopn to equip
			GameManager.gm.player.hasCannon = true;

			//Sets the inventory buttons
			GameManager.gm.userInterfaceManager.SetWeaponsInInvetory ();
		}
	}
}
