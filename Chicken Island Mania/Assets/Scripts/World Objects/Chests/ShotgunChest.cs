﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunChest : MonoBehaviour 
{
	void OnTriggerEnter (Collider other) 
	{
		if (other.gameObject.CompareTag ("Player"))
		{
			//Gives the player the shotgun weapon to equip
			GameManager.gm.player.hasShotgun = true;

			//Sets the inventory buttons
			GameManager.gm.userInterfaceManager.SetWeaponsInInvetory ();
		}
	}
}
