﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlamethrowerChest : MonoBehaviour 
{
	void OnTriggerEnter (Collider other) 
	{
		if (other.gameObject.CompareTag ("Player"))
		{
			//Gives the player the flamethrower weapon to equip
			GameManager.gm.player.hasFlamethrower = true;

			//Sets the inventory buttons
			GameManager.gm.userInterfaceManager.SetWeaponsInInvetory ();
		}
	}
}
