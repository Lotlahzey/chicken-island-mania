﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperChest : MonoBehaviour 
{
	void OnTriggerEnter (Collider other) 
	{
		if (other.gameObject.CompareTag ("Player"))
		{
			//Gives the player the sniper weapon to equip
			GameManager.gm.player.hasSniper = true;

			//Sets the inventory buttons
			GameManager.gm.userInterfaceManager.SetWeaponsInInvetory ();
		}
	}
}
