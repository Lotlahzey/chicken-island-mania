﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : MonoBehaviour 
{
	//Kills player upon entering
	void OnTriggerEnter (Collider other) 
	{
		if (other.gameObject.CompareTag ("Player")) 
		{
			GameManager.gm.KillPlayer ();
		}
	}
}
