﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour 
{
	public AudioClip deathSound;

	public NavMeshAgent agent;
	public Vector3 targetPosition;

	public float health;

	void Start () {
		agent = GetComponent<NavMeshAgent> ();

		//Adds self to list of active enemies
		GameManager.gm.enemyList.Add (this);

		//Health increases as player kills more enemies
		//Base health of 1 at score 0
		health = (GameManager.gm.player.score + 2) / 2;
	}
	
	void Update () 
	{
		//Ai movement
		targetPosition = GameManager.gm.player.transform.position;
		ChaseTarget ();

		//Ai destruction
		if (health <= 0) 
		{
			//Death sound
			AudioSource.PlayClipAtPoint (deathSound, gameObject.transform.position, GameManager.gm.audioManager.soundEffectsVolume * 5);

			//Score counter
			GameManager.gm.player.score++;

			//Removes self from list of active enemies
			GameManager.gm.enemyList.Remove (this);

			Destroy (gameObject);
		}
	}
		
	void ChaseTarget () 
	{
		agent.SetDestination (targetPosition);
	}

	void OnTriggerEnter (Collider other) 
	{
		//If enemy is hit by a chicken
		if (other.gameObject.CompareTag ("Chicken")) 
		{
			//Enemy takes damage
			Chicken chicken = other.gameObject.GetComponent<Chicken> ();
			health -= chicken.damage;
		}
		//If enemy hits a player
		if (other.gameObject.CompareTag ("Player")) 
		{
			//Kills the player
			GameManager.gm.KillPlayer ();
		}
	}
}
