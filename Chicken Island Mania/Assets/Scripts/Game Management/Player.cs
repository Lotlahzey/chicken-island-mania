﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Player : MonoBehaviour 
{
	public Camera playerCamera;

	[Space(5)]
	public AudioClip gunSound;

	[Header("Weapons")]
	[Space(2)]
	//Holds weapon prefabs
	public Pistol chickenPistol;
	public Flamethrower chickenFlinger;
	public Sniper chickenSniper;
	public Shotgun chickenBlaster;
	public Cannon chickenBomber;

	[Header("Stats")]
	[Space(2)]
	//Player stats
	public int score = 0;
	public int lives = 1;

	[HideInInspector]
	public bool hasPistol = true;
	[HideInInspector]
	public bool hasFlamethrower = false;
	[HideInInspector]
	public bool hasSniper = false;
	[HideInInspector]
	public bool hasShotgun = false;
	[HideInInspector]
	public bool hasCannon = false;
	[HideInInspector]
	public Vector3 startPosition;

	void Start () 
	{
		//Sets position for player to respawn
		startPosition = gameObject.transform.position;
	}

	//Equips pistol and unequips all other weapons
	public void EquipPistol () 
	{
		chickenFlinger.gameObject.SetActive (false);
		chickenSniper.gameObject.SetActive (false);
		chickenBlaster.gameObject.SetActive (false);
		chickenBomber.gameObject.SetActive (false);
		chickenPistol.gameObject.SetActive (true);
	}

	//Equips flamethrower and unequips all other weapons
	public void EquipFlamethrower () 
	{
		chickenPistol.gameObject.SetActive (false);
		chickenSniper.gameObject.SetActive (false);
		chickenBlaster.gameObject.SetActive (false);
		chickenBomber.gameObject.SetActive (false);
		chickenFlinger.gameObject.SetActive (true);
	}

	//Equips sniper and unequips all other weapons
	public void EquipSniper () 
	{
		chickenFlinger.gameObject.SetActive (false);
		chickenPistol.gameObject.SetActive (false);
		chickenBlaster.gameObject.SetActive (false);
		chickenBomber.gameObject.SetActive (false);
		chickenSniper.gameObject.SetActive (true);
	}

	//Equips shotgun and unequips all other weapons
	public void EquipShotgun () 
	{
		chickenFlinger.gameObject.SetActive (false);
		chickenSniper.gameObject.SetActive (false);
		chickenPistol.gameObject.SetActive (false);
		chickenBomber.gameObject.SetActive (false);
		chickenBlaster.gameObject.SetActive (true);
	}

	//Equips cannon and unequips all other weapons
	public void EquipCannon () 
	{
		chickenFlinger.gameObject.SetActive (false);
		chickenSniper.gameObject.SetActive (false);
		chickenBlaster.gameObject.SetActive (false);
		chickenPistol.gameObject.SetActive (false);
		chickenBomber.gameObject.SetActive (true);
	}
}
