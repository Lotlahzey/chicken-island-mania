﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{
	public static GameManager gm;
	public Player player;
	public UserInterfaceManager userInterfaceManager;
	public AudioManager audioManager;
	public List<EnemyController> enemyList;

	void Awake () 
	{
		//Makes the game manager a Singleton object
		if (gm == null) {
			gm = this;
		} else {
			Destroy (gameObject);
		}
	}

	void Start () 
	{
		//Timescale is set to 0 at startup to keep enemies from spawning while player is in start menu 
		Time.timeScale = 0;
	}
	
	void Update () 
	{
		//Opens inventory with 'I' key press
		if (Input.GetKeyDown (KeyCode.I)) 
		{
			if (player.gameObject.activeInHierarchy) 
			{
				GameManager.gm.OpenInventory ();
			} 
			else if (!player.gameObject.activeInHierarchy) 
			{
				GameManager.gm.CloseInventory ();
			}
		}

		//Opens pause menu with 'Escape' key press
		if (Input.GetKeyDown (KeyCode.Escape)) 
		{
			//Opens pause menu if not already open
			if (player.gameObject.activeInHierarchy) 
			{
				GameManager.gm.OpenPauseMenu ();
			} 
			//Closes all menu's and resumes game
			else if (!player.gameObject.activeInHierarchy) 
			{
				GameManager.gm.ClosePauseMenu ();
			}
		}
	}

	//Game starts
	public void StartGame () 
	{
		Time.timeScale = 1;
		userInterfaceManager.SetGameplayScreen ();
		player.gameObject.SetActive (true);
	}

	//Game ends
	public void GameOver () 
	{
		userInterfaceManager.SetGameOverScreen ();
		player.gameObject.SetActive (false);
		Time.timeScale = 0;
	}

	//Opens the player's inventory
	public void OpenInventory ()
	{
		userInterfaceManager.SetInventoryScreen ();
		player.gameObject.SetActive (false);
		Time.timeScale = 0;
	}

	//Closes the player's inventory
	public void CloseInventory ()
	{
		Time.timeScale = 1;
		GameManager.gm.userInterfaceManager.SetGameplayScreen ();
		player.gameObject.SetActive (true);
	}

	//Opens the pause menu
	public void OpenPauseMenu () 
	{
		userInterfaceManager.SetPauseScreen ();
		player.gameObject.SetActive (false);
		Time.timeScale = 0;
	}

	//Closes the pause menu
	public void ClosePauseMenu ()
	{
		Time.timeScale = 1;
		GameManager.gm.userInterfaceManager.SetGameplayScreen ();
		player.gameObject.SetActive (true);
	}

	//Opens the options menu
	public void OpenOptionsMenu ()
	{
		userInterfaceManager.SetOptionsScreen ();
	}

	//Closes the options menu
	public void CloseOptionsMenu ()
	{
		userInterfaceManager.SetPauseScreen ();
	}

	//Kills the player and resets their position
	public void KillPlayer () 
	{
		//Removes a life
		player.lives--;

		//If player looses all lives, game ends
		if (player.lives == 0) 
		{
			GameOver ();
		} 
		else 
		{
			//Resets player to starting position
			player.gameObject.SetActive (false);
			player.gameObject.transform.position = player.startPosition;
			player.gameObject.SetActive (true);

			//Removes all active enemies from the game
			foreach (EnemyController enemy in enemyList) 
			{
				Destroy (enemy.gameObject);
			}

		}
	}

	//Restarts the game
	public void Restart ()
	{
		//Removes all active enemies from the game
		foreach (EnemyController enemy in enemyList) 
		{
			Destroy (enemy.gameObject);
		}

		//Resets game stats back to default
		player.score = 0;
		player.lives = 3;

		//Removes weapons from inventory
		player.hasSniper = false;
		player.hasFlamethrower = false;
		player.hasShotgun = false;
		player.hasCannon = false;

		//Resets player to starting position
		player.gameObject.transform.position = player.startPosition;
		player.gameObject.SetActive (true);

	}
		
	//Exits the game
	public void Exit ()
	{
		Application.Quit ();
	}
}
